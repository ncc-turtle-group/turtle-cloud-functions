import * as functions from "firebase-functions";

import gm from "gm"
import fs from "fs";
import os from "os";
import path from "path";
import {spawn} from "child-process-promise";
import firebase from "firebase-admin";
import {BoundingBox, Image} from "../utils/types";
import automl from "@google-cloud/automl"
import {File} from "firebase-admin/node_modules/@google-cloud/storage" // Fix for TS:2345

const uploadedDirectory = 'image-uploads';
const predictedDirectory = 'predicted-images';
const thumbnailDirectory = 'image-thumbnails';

const database = firebase.database();
const imagesRef = database.ref('active-mission/uploaded-images');
const predictedImagesRef = database.ref(`active-mission/${predictedDirectory}`);
const bucket = firebase.storage().bucket(); // Project's default bucket
const predictionClient = new automl.PredictionServiceClient(); // Create client for prediction service

async function downloadFromBucket(uploadedStoragePath: string) {
	const uploadedFile = bucket.file(uploadedStoragePath); // Reference to image's file
	const uploadedName = path.basename(uploadedStoragePath);
	const uploadedFilepath = path.join(os.tmpdir(), uploadedName);
	await uploadedFile.download({destination: uploadedFilepath});
	return {uploadedFile, uploadedName, uploadedFilepath};
}

async function predict_image(filePath: string) {
	const projectId = 'turtle-cloud';
	const computeRegion = 'us-central1';
	const modelId = 'IOD5857848308092895232'; // https://console.cloud.google.com/vision/models
	const scoreThreshold = '0.7';

	// Read the file content for translation.
	const content = fs.readFileSync(filePath);

	// Create prediction request
	const request = {
		name: predictionClient.modelPath(projectId, computeRegion, modelId),
		payload: {
			image: {
				imageBytes: content,
			},
		},
		params: {
			score_threshold: scoreThreshold,
		},
	};

	// Request predictions from prediction client
	const [response] = await predictionClient.predict(request);

	// Extract bounding boxes from the prediction
	const boxes: BoundingBox[] = [];
	for (const annotationPayload of response.payload!!) {
		const normalizedVertices = annotationPayload.imageObjectDetection!!.boundingBox!!.normalizedVertices!!
		const box: BoundingBox = {
			label: annotationPayload.displayName!!,
			score: annotationPayload.imageObjectDetection!!.score!!,
			xmin: normalizedVertices[0].x!!,
			ymin: normalizedVertices[0].y!!,
			xmax: normalizedVertices[1].x!!,
			ymax: normalizedVertices[1].y!!
		}
		// console.log(box)
		boxes.push(box)
	}

	return boxes;
}

async function draw_bounding_boxes(boundingBoxes: BoundingBox[], imageFilepath: string): Promise<string> {
	const dir = path.dirname(imageFilepath)
	const predictedFilename = 'predicted_' + path.basename(imageFilepath)
	const predictedPath = path.join(dir, predictedFilename)

	return new Promise((resolve, reject) => {
		gm(imageFilepath).size(((err, value) => {
			if (err)
				throw Error('Error reading image size: ' + imageFilepath)
			else
				console.log(`Image size: ${value.width}x${value.height}`)

			let canvas = gm(imageFilepath);
			boundingBoxes.forEach(box => {
				const xmin_abs = box.xmin * value.width;
				const ymin_abs = box.ymin * value.height;
				const xmax_abs = box.xmax * value.width;
				const ymax_abs = box.ymax * value.height;

				console.log(`Bounding box: (${xmin_abs},${ymin_abs})x(${xmax_abs},${ymax_abs})`)
				canvas = canvas
					.stroke('red', 2) // Color & width of the box
					.drawRectangle(xmin_abs, ymin_abs, xmax_abs, ymax_abs, 4, 4) // box with round corners
					.fill('cyan')
					.drawText(xmin_abs, ymin_abs, `${box.label}: ${box.score}%`)
			})
			console.log(`Canvas write ${predictedPath}`)
			canvas.write(predictedPath, function (error) {
				if (error) {
					console.log('Error drawing')
					reject(error);
				} else {
					console.log('Drawing successful')
					resolve(predictedPath)
				}
			});
		}))
	})
}

async function generate_thumbnail(imageFilepath: string, contentType: string) {
	// Generate a thumbnail using ImageMagick.
	const imageName = path.basename(imageFilepath)
	const thumbFileName = `thumb_${imageName}`;
	const thumbFilepath = path.join(path.dirname(imageFilepath), thumbFileName)
	console.log('b4 resize')
	gm(imageFilepath)
		.resize(240, 240)
		.noProfile()
		.write(thumbFilepath, function (err) {
			if (err)
				throw err;
		});
	// await spawn('convert', [imageFilepath, '-thumbnail', '256x256', thumbFilepath]);
	console.log('after resize')
	const thumbStoragePath = path.join(thumbnailDirectory, thumbFileName);

	console.log(thumbStoragePath)
	// Uploading the thumbnail.
	const thumbUploadResponse = await bucket.upload(imageFilepath, {
		destination: thumbStoragePath,
		metadata: {
			contentType: contentType, // store contentType: image/ as metadata
		},
	});
	console.log('b4 unlink')

	// console.log(`Thumbnail created at: ${thumbStoragePath}`);
	fs.unlinkSync(thumbFilepath); // Clear up the thumbnail from local storage
	const thumbFile = thumbUploadResponse[0];

	return {thumbStoragePath, thumbFile};
}

async function uploadPredictedImage(imageFile: File, predictedPath: string, thumbStoragePath: string,
                                    thumbFile: File, turtleCount: number) {
	// First check if it has a metadata
	if (!imageFile.metadata) {
		console.error(`Image ${imageFile.name} has no metadata`);
		return
	}

	// Then check if the metadata contains a location
	if (!imageFile.metadata['location']) {
		console.error(`Image ${imageFile.name} has no location metadata`);
		return
	}


	const predictedName = path.basename(predictedPath)
	const predictedStoragePath = path.join(predictedDirectory, predictedName);

	// Upload predicted file to bucket
	const predictedFile = (await bucket.upload(predictedPath, {
		destination: predictedStoragePath,
		metadata: {
			contentType: 'image/', // Will it work?
		},
	}))[0];
	fs.unlinkSync(predictedPath); // Original image is processed, clear from local storage

	// save the thumbnail's path to image file's metadata,
	// in order to delete corresponding thumbnail
	// whenever image file itself is deleted
	await predictedFile.setMetadata({
		metadata: {
			'thumbnail-path': thumbStoragePath
		}
	})

	// Generate public URLs
	// TODO: Signed URL is a bad idea. Replace with GCP IAM later on. Hint: use mediaLinks
	const imageURL = await imageFile.getSignedUrl({
		action: 'read',
		expires: '01-01-2025'
	});

	// Returns array of strings, so don't forget to index it
	const thumbURL = await thumbFile.getSignedUrl({
		action: 'read',
		expires: '01-01-2025'
	});
	console.log(imageFile.metadata['location'])
	// Finally, push new image reference with all the necessary properties
	const predictedRef = await predictedImagesRef.push({
		thumbnail: thumbURL[0],
		source: imageURL[0],
		timestamp: Date.now(),
		location: JSON.parse(imageFile.metadata['location']),
		turtleCount: turtleCount
	} as Image);
	console.log(`Image reference in the database was created with key:${predictedRef.key}`);
}

/**
 * When an image is uploaded in the Storage bucket,
 * We generate a thumbnail automatically using ImageMagick.
 */
// TODO: Parameterize region
export const uploadPicture = functions.region('europe-west1').storage.object()
	.onFinalize(async (object) => {
		const uploadedStoragePath = object.name as string; // File path in the bucket.
		const contentType = object.contentType as string; // File content type.

		console.log(`Object ${object.name} was uploaded`);

		// Exit if it is different kind of upload
		if (path.dirname(uploadedStoragePath) !== uploadedDirectory) {
			console.log(`Ignoring images outside of ${uploadedDirectory} directory`)
			return;
		}

		// Exit if this is triggered on a file that is not an image.
		if (!contentType.startsWith('image/')) {
			console.log(`Ignoring non-image uploads: ${uploadedStoragePath}`);
			return;
		}

		console.log(`Downloading the image from bucket: ${uploadedStoragePath}`)
		const {uploadedFile, uploadedName, uploadedFilepath} = await downloadFromBucket(uploadedStoragePath);

		// Run prediction
		console.log(`Running predictions on the image: ${uploadedName}`)
		const boundingBoxes = await predict_image(uploadedFilepath);

		// Draw bounding boxes
		// console.log(`Drawing ${boundingBoxes.length} bounding boxes on image: ${uploadedName}`)
		// const predictedPath = await draw_bounding_boxes(boundingBoxes, uploadedFilepath)
		// fs.unlinkSync(uploadedFilepath); // Original image is processed, clear from local storage

		// console.log('Generating thumbnail for predicted image: ' + path.basename(predictedPath));
		// const {thumbStoragePath, thumbFile} = await generate_thumbnail(predictedPath, contentType);

		// console.log('Uploading the predicted image: ' + path.basename(predictedPath));
		// await uploadPredictedImage(uploadedFile, predictedPath, thumbStoragePath, thumbFile, boundingBoxes.length);
		// fs.unlinkSync(predictedPath); // Predicted image is uploaded, clear from local storage

		// console.log('Deleting uploaded image: ' + uploadedName)
		// await uploadedFile.delete()
		const {thumbStoragePath, thumbFile} = await generate_old_thumbnail(uploadedFilepath, object.contentType!!)

		await updateDatabase(uploadedFile, thumbFile, thumbStoragePath, boundingBoxes)
	});

async function updateDatabase(uploadedFile: File, thumbFile: File, thumbStoragePath: string, boxes: BoundingBox[]) {
	await uploadedFile.setMetadata({
		metadata: {
			'thumbnail-path': thumbStoragePath
		}
	})

	// Generate public URLs
	// TODO: Signed URL is a bad idea. Replace with GCP IAM later on. Hint: use mediaLinks
	const imageURL = await uploadedFile.getSignedUrl({
		action: 'read',
		expires: '01-01-2025'
	});

	// Returns array of strings, so don't forget to index it
	const thumbURL = await thumbFile.getSignedUrl({
		action: 'read',
		expires: '01-01-2025'
	});
	const imageRef = await imagesRef.push({
		thumbnail: thumbURL[0],
		source: imageURL[0],
		timestamp: Date.now(),
		location: JSON.parse(uploadedFile.metadata.metadata['location']),
		bounding_boxes: boxes
	} as Image);
	console.log(`Image reference in the database was created with key:${imageRef.key}`);
}

async function generate_old_thumbnail(imageFilepath: string, contentType: string) {
	// Generate a thumbnail using ImageMagick.
	const imageName = path.basename(imageFilepath)
	const thumbFileName = `thumb_${imageName}`;
	const thumbFilepath = path.join(path.dirname(imageFilepath), thumbFileName)
	await spawn('convert', [imageFilepath, '-thumbnail', '256x256', thumbFilepath]);
	const thumbStoragePath = path.join(thumbnailDirectory, thumbFileName);

	// Uploading the thumbnail.
	const thumbUploadResponse = await bucket.upload(thumbFilepath, {
		destination: thumbStoragePath,
		metadata: {
			contentType: contentType, // store contentType: image/ as metadata
		},
	});

	// console.log(`Thumbnail created at: ${thumbStoragePath}`);
	fs.unlinkSync(thumbFilepath); // Clear up the thumbnail from local storage
	const thumbFile = thumbUploadResponse[0];

	return {thumbStoragePath, thumbFile};
}


// TODO: Parameterize region
export const deletePicture = functions.region('europe-west1').storage.object()
	.onDelete(async (object) => {
		const imagePath = object.name as string; // File path in the bucket.
		// Exit if the deleted object is an uploaded picture
		if (path.dirname(imagePath) !== predictedDirectory)
			return;

		console.log(`Deleting image ${imagePath} ...`);

		// No metadata, hence can't do anything
		if (!object.metadata) {
			console.error(`Image ${imagePath} has no metadata. Cannot delete corresponding reference.`);
			return;
		}

		// Check if thumbnail path is present in the metadata
		const thumbPath = object.metadata['thumbnail-path'];
		if (!thumbPath) {
			console.error(`Image ${imagePath} has no thumbnail path`);
			return;
		}

		// Check if the thumbnail object is present
		const thumbnailFile = bucket.file(thumbPath);
		if (!await thumbnailFile.exists()) {
			console.error(`Thumbnail ${thumbPath} does not exist`);
			return
		}

		// Finally, delete the corresponding thumbnail
		await thumbnailFile.delete();
		console.log(`Image's thumbnail at ${thumbPath} was successfully deleted`);
	});