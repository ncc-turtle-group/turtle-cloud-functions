// Firebase initialization must be the first thing to do in the app.
require("./firebase-init");
import * as functions from "firebase-functions";
import express from "express";
import piCommandsRouter from "./routes/pi-commands";
import mockRouter from "./routes/mock";
import itemsRouter from "./routes/items";
import imageUploadsRouter from "./routes/image-uploads";
import {uploadPicture, deletePicture} from "./triggers/storage-triggers";


const cors = require('cors');
const app = express();
app.use(cors({origin: true}));

app.use('/api/pi-commands', piCommandsRouter);
app.use('/api/mock', mockRouter);
app.use('/api/items', itemsRouter);
app.use('/api/image-uploads', imageUploadsRouter);


// TODO: 404 route

exports.app = functions
	.region('europe-west1')
	.https.onRequest(app);

exports.uploadPicture = uploadPicture;
exports.deletePicture = deletePicture;