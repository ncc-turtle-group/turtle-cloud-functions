export enum CommandState {
	Pending,
	Running,
	Completed,
	Canceling,
	Canceled
}

export interface Command {
	id: number,
	operation: string,
	state: CommandState,
	status: string
	timestamp: number,
}

export interface GPSLocation {
	latitude: number,
	longitude: number
}

export interface Image {
	thumbnail: string,
	source: string,
	timestamp: number,
	location: GPSLocation
}

export interface BoundingBox {
	label: string,
	score: number,
	xmin: number,
	ymin: number,
	xmax: number,
	ymax: number
}