import {Request, Response} from "express";
import * as response from "./responses"

/**
 * Thunk for wrapping error handling.
 * Meant to be used as an argument to express get/post/etc...methods
 * */
export function requestHandler(handler: (req: Request, res: Response) => Promise<Response>) {
	return async (req: Request, res: Response) => {
		try {
			return await handler(req, res);
		} catch (error) {
			return response.error(res, error)
		}
	}
}

