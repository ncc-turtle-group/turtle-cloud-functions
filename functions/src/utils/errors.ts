export class BadRequestError extends Error {

}

export class ParameterMissingError extends BadRequestError {
	constructor(parameter: string) {
		super(`Parameter '${parameter}' is missing in the request`);
		this.name = 'ParameterMissingError'
	}
}