import {Request} from "express";
import {ParameterMissingError} from "./errors";

/**
 * Checks if all expected query parameters were provided in the request
 * @throws {@link ParameterMissingError} if any of the parameters is missing
 */
export function validateRequestParams(req: Request, ...expectedParams: string[]) {
	expectedParams.forEach(param => {
		if (!req.query[param])
			throw new ParameterMissingError(param);
	});
}