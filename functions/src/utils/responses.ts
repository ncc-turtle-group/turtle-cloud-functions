import {Response} from "express";
import {BadRequestError} from "./errors";

export const ok = (res: Response, message: any) => res.status(200).send(message);

/**
 * Returns message with appropriate response code:
 * 400 - Bad Request
 * 500 - Internal Server Error
 * */
export const error = (res: Response, err: Error) => {
	if (err instanceof BadRequestError) {
		// Client error
		return res.status(400).send(err.message);
	} else {
		// Server error
		console.error(err.message); // Log the error internally, for debugging
		return res.status(500).send(err.message);
	}
};