const fetch = require("node-fetch");

async function fetchURLFromReddit(subreddit: string) {
	const response = await fetch(subreddit);
	const json = await response.json();
	return json.data?.children
		.map((post: any) => post.data.url as string)
		.filter((url: string) => (url.endsWith('.png') || url.endsWith('.jpg'))) ?? [];
}

export async function getRandomMemeURL() {
	const set1 = await fetchURLFromReddit("https://www.reddit.com/r/darkmeme.json?limit=100");
	const set2 = await fetchURLFromReddit("https://www.reddit.com/r/dankmemes.json?limit=100");

	const combinedSet = set1.concat(set2);

	// Get random member of the combined array
	return combinedSet[Math.floor(Math.random() * combinedSet.length)]
}