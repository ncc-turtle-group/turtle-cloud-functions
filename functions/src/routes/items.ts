/**
 * Those functions are here just for reference,
 * in case we will need to use cloud items
 * */

import {Router} from "express";
import firebase from "firebase-admin";
import * as response from "../utils/responses"
import {requestHandler} from "../utils/handlers";

const firestore = firebase.firestore();
const itemsRef = firestore.collection('items');

// Init router
const router = Router();
export default router;

// Create item
router.post('/create', requestHandler(async (req, res) => {
	await itemsRef
		.doc(`/${req.body.id}/`) // Reference to the new doc
		.create({item: req.body.item}); // Init doc with provided item
	return response.ok(res, "successful create");
}));

// Read specific item
router.get('/read/:item_id', requestHandler(async (req, res) => {
	const document = itemsRef.doc(req.params.item_id);
	const itemRef = await document.get();
	const itemData = itemRef.data();
	return response.ok(res, itemData);
}));

// Read all items
router.get('/read', requestHandler(async (req, res) => {
	const querySnapshot = await itemsRef.get();

	const items = querySnapshot.docs.map(doc => ({
		[doc.id]: doc.data()
	}));

	return response.ok(res, items);
}));

// Update item
router.patch('/update/:item_id', requestHandler(async (req, res) => {
	const itemRef = itemsRef.doc(req.params.item_id);
	await itemRef.update({
		item: req.body.item
	});
	return response.ok(res, `Item id:${req.params.item_id} was successfully updated`);
}));

// Delete item
router.delete('/delete/:item_id', requestHandler(async (req, res) => {
	const document = itemsRef.doc(req.params.item_id);
	await document.delete();
	return response.ok(res, `Item id:${req.params.item_id} was successfully deleted`);
}));
