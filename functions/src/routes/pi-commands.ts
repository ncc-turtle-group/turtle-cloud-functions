import firebase from "firebase-admin";
import {Router} from "express";
import * as response from "../utils/responses"
import * as validations from "../utils/validations"
import {Command, CommandState} from "../utils/types";
import {requestHandler} from "../utils/handlers"
import {BadRequestError} from "../utils/errors";
import DataSnapshot = firebase.database.DataSnapshot;

// Get reference to the root of the database
const database = firebase.database();

// Get reference to pi commands
const piCommandsRef = database.ref("piCommands");
const commandQueueRef = piCommandsRef.child("commandQueue");

// Select next incomplete command
// Since commands are stored chronologically,
// First result will always point to the longest waiting command
async function nextIncompleteCommand() {
	const nextCommands = await commandQueueRef
		.orderByChild('completed')
		.equalTo(false)
		.limitToFirst(1)
		.once('value');

	// Check if there are any commands present
	if (!nextCommands.exists())
		return null;

	return nextCommands
}

// Init router
const router = Router();
export default router;

// Create command
router.post('/create', requestHandler(async (req, res) => {
	// Check if the 'operation' parameter was provided
	validations.validateRequestParams(req, 'operation');

	// Get unique counter via transaction, since we want counter to be synchronized
	const transactionResult = await piCommandsRef.child('counter').transaction(
		(counter) => counter + 1
	);

	// If transaction was successful, but snapshot is null
	if (!transactionResult.snapshot)
		return response.error(res, new Error("Snapshot is null"));

	// Unbox the counter value
	const commandCounter = transactionResult.snapshot.val();

	// Add new command to the pool using push operation,
	// since we want it to be added with unique reference key
	const command = await commandQueueRef.push({
		id: commandCounter,
		operation: req.query.operation,
		state: CommandState.Pending,
		status: "Pending...",
		timestamp: Date.now(),
	} as Command);

	return response.ok(res,
		`Command with id:${commandCounter} was successfully added to the command pool.` +
		`\nReference id:${command.key}`);
}));

// Read all commands
router.get('/read', requestHandler(async (req, res) => {
	const allCommandsSnapshot = await commandQueueRef.once('value');
	return response.ok(res, allCommandsSnapshot)
}));

// Read specific command
router.get('/read/:id', requestHandler(async (req, res) => {
	// Get id parameter from the request url
	const id = +req.params.id; // unary `+` operator converts string to number

	// Validate that this parameter is a number
	if (isNaN(id))
		return response.error(res, new BadRequestError(
			`/read/:id endpoint expects numeric id. Received id:${req.params.id}`
		));

	const command = await commandQueueRef
		.orderByChild('id')
		.equalTo(id)
		.once('value');

	if (command.exists())
		return response.ok(res, command.val());
	else
		return response.error(res, new BadRequestError(`Command with id:${id} does not exist`))
}));

// Read next command to be processed
router.get('/read-next-incomplete', requestHandler(async (req, res) => {
	const nextCommand = await nextIncompleteCommand();

	// Check if the next command(s) exist(s)
	if (!nextCommand)
		return response.ok(res, `The command queue is empty, nothing to read`);

	return response.ok(res, nextCommand)
}));

// Cancel command
router.patch('/cancel', requestHandler(async (req, res) => {
	// Check if the 'commandRef' parameter was provided
	validations.validateRequestParams(req, 'commandRef');

	// Get reference to command
	const commandRef = commandQueueRef.child(req.query.commandRef as string);

	// Query the command object by reference id
	const commandSnapshot = await commandRef.once('value');

	// Check if the command with provided reference exists
	if (!commandSnapshot.exists())
		return response.error(res, new BadRequestError(
			`Command with reference:${req.query.commandRef} does not exist`
		));

	// Unbox the Command object
	const command = commandSnapshot.val() as Command;

	// Check if command was already requested to cancel

	switch (command.state) {
		case CommandState.Canceled:
		case CommandState.Canceling:
		case CommandState.Completed:
			return response.error(res, new BadRequestError(
				`Command with id:${command.id} is already ${command.state}`
			));
	}


	// Finally, request the command to be canceled
	await commandRef.update({
		state: CommandState.Canceling,
	} as Command);

	return response.ok(res, `Command with id:${command.id} was successfully requested to canceled`)
}));

// Cancel first running commands
router.patch('/cancel-running-command', requestHandler(async (req, res) => {
	let snapshot = await commandQueueRef
		.orderByChild('state')
		.equalTo(CommandState.Running)
		.limitToFirst(1)
		.once('value');

	if (!snapshot.exists())
		return response.ok(res, 'No commands are currently running');

	snapshot.forEach((commandSnapshot: DataSnapshot) => {
		// As long as there is 1 item in the snapshot list, should be fine
		snapshot = commandSnapshot
	});

	await snapshot.ref.update({
		'state': CommandState.Canceling,
		'status': 'Canceling...'
	});

	const id = snapshot.val().id;
	return response.ok(res, `Command with id:${id} has been cancelled`)
}));

// Delete command
router.delete('/delete', requestHandler(async (req, res) => {
	// Check if the 'commandRef' parameter was provided
	validations.validateRequestParams(req, 'commandRef');
	const commandRefID = req.query.commandRef as string;

	// Get reference to command
	const commandRef = commandQueueRef.child(commandRefID);

	// Query the command object by reference id
	const commandSnapshot = await commandRef.once('value');

	// Check if the command with provided reference exists
	if (!commandSnapshot.exists())
		return response.error(res,
			new BadRequestError(`Command with reference:${commandRefID} does not exist`));

	// We delete the command by setting it to null
	await commandRef.set(null);

	return response.ok(res, `Command with reference:${commandRefID} was successfully deleted`)
}));

// Delete next incomplete command
router.delete('/delete-next-incomplete', requestHandler(async (req, res) => {
	const nextCommandSnapshot = await nextIncompleteCommand();

	if (!nextCommandSnapshot)
		return response.ok(res, `The command queue is empty, nothing to delete`);

	// Extract the command's reference
	const nextCommandKey = Object.keys(nextCommandSnapshot.toJSON() as object)[0];

	const nextCommandRef = nextCommandSnapshot.child(nextCommandKey).ref;

	await nextCommandRef.ref.set(null);

	return response.ok(res, `Command with reference:${nextCommandKey} was successfully deleted`)
}));
