// Get reference to the root of the database
import firebase from "firebase-admin";
import {requestHandler} from "../utils/handlers";

import * as response from "../utils/responses";
import {Router} from "express";

const storage = firebase.storage();

const uploadsDir = 'image-uploads/';
const bucket = storage.bucket();

// Get reference to the root of the database
const database = firebase.database();
const piCommandsRef = database.ref("piCommands");

// Init router
const router = Router();
export default router;

/**
 * Deletes all images in the uploads folder
 * */
router.get('/delete-all', requestHandler(async (req, res) => {
	// Delete Files
	await bucket.deleteFiles({prefix: uploadsDir, delimiter: '/'});

	// Reset counter and commands
	await database.ref('piCommands').remove();

	// Return response
	const message = 'Deleted all pics from: ' + uploadsDir;
	console.log(message);
	return response.ok(res, message)
}));
