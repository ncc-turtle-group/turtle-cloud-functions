// Get reference to the root of the database
import firebase from "firebase-admin";
import {requestHandler} from "../utils/handlers";

import * as response from "../utils/responses";
import {Router} from "express";
import {GPSLocation} from "../utils/types";
import path from "path";
import os from "os";
import {getRandomMemeURL} from "../utils/meme-fetcher";

const download = require('image-downloader');
const storage = firebase.storage();

const storageDir = 'image-uploads/';
const bucket = storage.bucket();

// Init router
const router = Router();
export default router;

/**
 * Uploads provided image to Firebase storage.
 * */
router.get('/image-upload', requestHandler(async (req, res) => {

	// Step 1. Get rng pic url
	const rngPic = await getRandomMemeURL();
	const imageName = rngPic.split('/').pop();
	const tempImgPath = path.join(os.tmpdir(), imageName);

	// Step 2. Download rng image

	// Suppress this warning, since image-downloader package does not have TS definitions
	// noinspection TypeScriptValidateJSTypes
	const {filename} = await download.image({
		url: rngPic,
		dest: tempImgPath
	});
	console.log('Downloaded rng pic to: ' + filename); // => /path/to/dest/image.jpg

	// Step 3. Upload the pic to bucket
	const bucketDestination = storageDir + Date.now() + '-' + imageName;
	const uploadResponse = await bucket.upload(filename, {
		destination: bucketDestination,
		metadata: {
			metadata: {
				location: JSON.stringify({
					latitude: Math.random() * 180 - 90,
					longitude: Math.random() * 360 - 180
				} as GPSLocation)
			}
		}
	});
	console.log('Uploaded pic to: ' + bucketDestination);
	return response.ok(res, `Successfully uploaded: ${uploadResponse[0].name}`)
}));
